using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThunderStrike : MonoBehaviour
{
    [SerializeField] private float shockwaveForce;
    [SerializeField] private float shockwaveRadius;
    [SerializeField] private float shockwaveDelay;

    private void Start()
    {
        StartCoroutine(ShockwaveDelay(shockwaveDelay));
    }

    private IEnumerator ShockwaveDelay(float _delay)
    {
        yield return new WaitForSeconds(shockwaveDelay);
        Shockwave();
    }

    private void Shockwave()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, shockwaveRadius);
        foreach (Collider collider in colliders)
        {
            IExplodable explodable = collider.GetComponent<IExplodable>();
            if (explodable != null)
            {
                explodable.OnExplode(shockwaveForce, transform.position - Vector3.up * 2, shockwaveRadius);
            }
        }

        Destroy(gameObject);
    }
}
