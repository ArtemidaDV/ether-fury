using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatingAround : MonoBehaviour
{
    [SerializeField] private Transform moveCenter;
    [SerializeField] private Transform rotateCenter;
    [SerializeField] private float moveSpeed;

    private float time;
    private float radius;

    private void Start()
    {
        time = 0;
        radius = (transform.position - moveCenter.position).magnitude;
    }

    private void Update()
    {
        time += Time.deltaTime * moveSpeed;

        transform.position = new Vector3(Mathf.Sin(time) * radius, transform.position.y, Mathf.Cos(time) * radius);
        transform.LookAt(rotateCenter);
    }
}
