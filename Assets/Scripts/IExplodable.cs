using UnityEngine;

interface IExplodable
{
    public abstract void OnExplode(float explosionForce, Vector3 explosionCenter, float explosionRadius); 
}