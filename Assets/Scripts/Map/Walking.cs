using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walking : MapObject
{
    [SerializeField] private float moveSpeed;
    [SerializeField] private float moveDistance;

    [SerializeField] private Vector3 target;
    [SerializeField] private bool isAlive = true;

    private void Start()
    {
        ChangeTarget();
    }

    private void FixedUpdate()
    {
        if (!isAlive) return;

        if ((transform.position - target).magnitude < 0.1f)
        {
            ChangeTarget();
        }

        rb.AddForce((target - transform.position).normalized * moveSpeed * Time.fixedDeltaTime);
    }

    private void OnCollisionEnter(Collision collision)
    {
        ChangeTarget();
    }

    private void ChangeTarget()
    {
        for (int i = 0; i < 100; i++) 
        {
            float direction = Random.Range(0, 2 * Mathf.PI);
            float x = Mathf.Cos(direction);
            float z = Mathf.Sin(direction);

            Vector3 newTarget = new Vector3(x, 0, z);
            if (!Physics.Raycast(transform.position, transform.TransformDirection(newTarget), moveDistance + 0.45f))
            {
                if (Physics.Raycast(transform.position + newTarget * moveDistance, transform.TransformDirection(-Vector3.up), 3))
                {
                    target = transform.position + newTarget * moveDistance;
                    return;
                }
            }
        }

        Debug.LogError("Walking agent can't find new target!");
    }
    protected override IEnumerator OnReset()
    {
        transform.localEulerAngles = Vector3.zero;
        rb.velocity = Vector3.zero;
        rb.isKinematic = true;

        yield return null;

        transform.position = startPosition;
        rb.isKinematic = false;
        rb.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
        isAlive = true;
    }

    public override void OnExplode(float explosionForce, Vector3 explosionCenter, float explosionRadius)
    {
        rb.constraints = RigidbodyConstraints.None;
        rb.AddExplosionForce(explosionForce, explosionCenter, explosionRadius);

        isAlive = false;
    }
}
