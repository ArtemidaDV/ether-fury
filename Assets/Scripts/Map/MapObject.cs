using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapObject : MonoBehaviour, IExplodable
{
    protected Vector3 startPosition;
    protected Rigidbody rb;

    private void Awake()
    {
        startPosition = transform.position;
        rb = GetComponent<Rigidbody>();
    }

    public void Reset()
    {
        StartCoroutine(OnReset());
    }

    protected virtual IEnumerator OnReset()
    {
        transform.localEulerAngles = Vector3.zero;
        rb.velocity = Vector3.zero;
        rb.isKinematic = true;

        yield return null;

        transform.position = startPosition;
        rb.isKinematic = false;
    }
    public virtual void OnExplode(float explosionForce, Vector3 explosionCenter, float explosionRadius) 
    {
        rb.AddExplosionForce(explosionForce, explosionCenter, explosionRadius);
    }
}
