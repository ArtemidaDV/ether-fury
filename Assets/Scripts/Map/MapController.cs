using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapController : MonoBehaviour
{
    private MapObject[] mapObjects;

    private void Awake()
    {
        mapObjects = FindObjectsByType<MapObject>(FindObjectsSortMode.None);
    }

    public void Reset()
    {
        foreach (MapObject mapObj in mapObjects) 
        { 
            mapObj.Reset();
        }
    }
}
